//
//  ViewController.swift
//  coffe_mateus
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cafe : Decodable {
    let file:String
}

class ViewController: UIViewController {
    var coffe_json:[Cafe] = []
    
    @IBOutlet weak var img_cafe: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNovoCafe()
        }
    @IBAction func recarregar(_ sender: Any) {
        getNovoCafe()
    }
    
    func getNovoCafe() {
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: Cafe.self){
            response in
            if let cafe = response.value{
                self.img_cafe.kf.setImage(with:URL(string:cafe.file))
            }
        }
    }



}

